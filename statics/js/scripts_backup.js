$(document).ready(() => {
  
  /************** DEFINE OUR DATA HOLDING CLASS **************/
  class Globals {
    /**
     * class that holds all the global variables 
     * (globals + state variables accessible by 
     * all functions in this scripts)
     *!
     * all variables, except for state loggers like 
     * `currentToggling` (which log temporary data)
     * should be defined as constants.
     *
     * the data is defined inside a class because
     * we need to fetch `iconographyData` asynchronously
     * => we use a `build()` method which acts like an
     * asynchronous constructor, assigning values to 
     * our data. this is to bypass the fact that a
     * javascript constructor cannot be asynchronous.
     * see: https://stackoverflow.com/questions/43431550/async-await-class-constructor
     */
    constructor(
        serverUrl
        , icnData
        , lflIds
        , icnIds
        , introIcnUuid
        , pipIds
        , d3Ids
        , navImages
        , windowHeight
        , animDur
    ) { 
      this.serverUrl = serverUrl;          // base url to the site on digital.inha.fr
      
      this.lflIds = lflIds;                // html ids of the map elements, which are keys of `lflData`
      this.currentLflId = lflIds[0];       // the html id of the map currently displayed
      this.lflViewer = undefined;          // leaflet object for the togglable map
      
      this.icnData = icnData;              // json data for our icono viewers
      this.icnIds = icnIds;                // html ids of the icono viewers, which are keys of `icnData`
      this.currentIcnId = icnIds[0];       // the html id of the icono viewer currently displayed
      this.introIcnUuid = introIcnUuid;    // the key of the image being displayed in the intro osd viewer
      this.icnViewer = undefined;          // openseadragon icono viewer; will be defined later
      
      this.pipIds = pipIds;                // same as the other ids
      this.currentPipId = pipIds[0]        // html id of the original pip viewer
      this.pipViewer = undefined;          // openseadragon pipeline viewer
      
      this.d3Ids = d3Ids;                  // html ids for the 3D/SIG images (`#modele3d`)
      this.currentD3Id = d3Ids[0];         // html id of the original d3 viewer
      this.d3Viewer = undefined;           // openseadragon viewer
      
      this.windowHeight = windowHeight;    // height of the html viewer
      this.navImages = navImages;          // icons for osd
      this.animDur = animDur               // animation duration in milliseconds. disables some animations on mobile devices to avoid jank
      this.currentToggling = false;        // state logger to log that a viewer/map toggling is currently ongoing
      this.currentIntroToggle = false;     // the intro map/menu is currently being toggled
      this.currentIntroDisplay = false;    // the entire intro view `$(".intro")` is currently being animated in/out  
    }
    
    static async build()  {
      /**
       * fetch and build all of our globals
       */
      const serverUrl = new URL("https://digital.inha.fr/richelieu/code/application_presentation/");
      const lflIds = [ "map-base", "map-iconography", "map-institution", "map-streets" ];
      const icnData = {
        "qrbaf24d7857ab45068737e09a81635771": {
          "img_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b53022027g/f1/full/full/0/native.jpg",
          "manifest_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b53022027g/manifest.json"
        },
        "qr6fffa8794c104be480eb5562e3ff71ac": {
          "img_url": "https://apicollections.parismusees.paris.fr/sites/default/files/styles/4k/collections/atoms/images/CAR/lpdp_79258-9.jpg?itok=OZYx4Nzk",
          "manifest_url": "https://apicollections.parismusees.paris.fr/iiif/320077754/manifest"
        },
        "qrcffe17d72bf8445ab02b582ca6cb694b": {
          "img_url": "https://gallica.bnf.fr/ark:/12148/btv1b53167734f/f1.highres",
          "manifest_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b53167734f/manifest.json"
        },
        "qrf5d07a3aeecd43bc97c2bdeaf1902f63": {
          "img_url": "https://www.parismuseescollections.paris.fr/sites/default/files/styles/pm_notice/public/atoms/images/CAR/aze_carph015716_001.jpg?itok=h6bnGV9J",
          "manifest_url": "https://apicollections.parismusees.paris.fr/iiif/320162854/manifest"
        },
        "bnf206": {
          "img_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b10516925w/f1/full/full/0/native.jpg",
          "manifest_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b10516925w/manifest.json"
        },
        "bnf216": {
          "img_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b530112408/f1/full/full/0/native.jpg",
          "manifest_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b530112408/manifest.json"
        },
        "bnf56": {
          "img_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b90082348/f1/full/full/0/native.jpg",
          "manifest_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b90082348/manifest.json"
        },
        "mc277": {
          "img_url": "https://apicollections.parismusees.paris.fr/sites/default/files/styles/4k/collections/atoms/images/CAR/lpdp_80039-7.jpg",
          "manifest_url": "https://apicollections.parismusees.paris.fr/iiif/320063615/manifest"
        },
        "mcv1": {
          "img_url": "https://apicollections.parismusees.paris.fr/sites/default/files/styles/4k/collections/atoms/images/CAR/aze_card14034_001.jpg",
          "manifest_url": "https://apicollections.parismusees.paris.fr/iiif/320002284/manifest"
        },
        "mcp8": {
          "img_url": "fr/sites/default/files/styles/4k/collections/atoms/images/CAR/lpdp_76149-1.jpg",
          "manifest_url": "https://apicollections.parismusees.paris.fr/iiif/320155148/manifest"
        },
        "bnf168": {
          "img_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b6952490n/f1/full/full/0/native.jpg",
          "manifest_url": "https://gallica.bnf.fr/iiif/ark:/12148/btv1b6952490n/manifest.json"
        }
      };
      const icnIds = Object.keys(icnData);
      const introIcnUuid = "qrf5d07a3aeecd43bc97c2bdeaf1902f63";
      const pipIds = [ "pipeline-richelieu", "data-model" ];
      const d3Ids = [ "cloud", "sig0", "sig1", "sig2", "sig3" ];
      
      const navImages = await loadStaticsFile("data/navimages.json");
      const windowHeight = $(window).height();
      const animDur = window.matchMedia("( min-width: 900px ) and ( orientation: landscape )").matches
                     ? 500  // not mobile => there will be animations
                     : 0;   // mobile => no animations
        
      return new Globals(
        serverUrl
        , icnData
        , lflIds
        , icnIds
        , introIcnUuid
        , pipIds
        , d3Ids
        , navImages
        , windowHeight
        , animDur
      );
    }
  }
  
  /* START `GLOBALS` BLOCK */
  Globals.build().then( async (globals) => {

    /************** GLOBAL LOGIC **************/
    /* DEFAULT BEHAVIOUR */
    // hide what needs to be hidden by default: legends, `.text-full` and unused `.more` buttons
    $(".viewer-legend > div").addClass("hide");
    $(".text-full").addClass("hide");
    $(`#cartographie .text-block > *
      , #iconographie .text-block > *
      , #pipeline .text-block > *`).addClass("hide");
    ( ! $("#iconographie").find(`.text-block div[title='${globals.currentIcnId}'] .text-full`).length )
    ? hideMore($("#iconographie"))
    : false;
    ( ! $("#pipeline").find(`.text-block div[title='${globals.currentPipId}'] .text-full`).length )
    ? hideMore($("#pipeline"))
    : false;
    
    // select the active menu element + load texts
    window.scrollTo({ top: 0, left: 0, behavior: "instant" });  // scroll back to top. avoids calculation errors for the intro block
    createMenu();
    selectMenu();
    initTextBlocks();
    svg().then( () => {
      buildIntro();
      window.matchMedia("not ( hover:hover )").matches ? introMenuIn() : false;
    });
    
    // define html id of the viewers + legends
    $(".map").attr("id", globals.currentLflId);
    $(`#cartographie .viewer-legend > div[title="${globals.currentLflId}"]`).removeClass("hide")
    $("#iconographie .osd-main").attr("id", globals.currentIcnId);
    $(`#iconographie .viewer-legend > div[title="${globals.currentIcnId}"]`).removeClass("hide")
    $("#modele3d .osd-main").attr("id", globals.currentD3Id);
    $(`#modele3d .viewer-legend > div[title="${globals.currentD3Id}"]`).removeClass("hide")
    $("#pipeline .osd-main").attr("id", globals.currentPipId);
    $(`#pipeline .viewer-legend > div[title="${globals.currentPipId}"]`).removeClass("hide")
    
    // define all viewers. use timeouts to avoid jank
    setTimeout( defineLfl, 1000);
    setTimeout(() => { defineOsd("icn") }, 1250);
    setTimeout(() => { defineOsd("d3") }, 1500);
    setTimeout(() => { defineOsd("pip") }, 1750);
    
    
    /* EVENTS */
    $(window).resize(() => {
      $(".intro-menu-container svg").css({ height: 0.8 * $(".intro-menu-container").height() });  // can't be done in css
      setTimeout(() => {
        buildIntro();
        globals.animDur = window.matchMedia("( min-width: 900px ) and ( orientation: landscape )").matches
                          ? 500
                          : 0;
      }, 1000);
    });
    $(".intro-menu-container").on("mouseenter", (e) => { 
      ! globals.currentIntroToggle
      ? introMenu(introMenuIn)
      : false;
    }).on("mouseleave", () => {
      ! globals.currentIntroToggle
      ? introMenu(introMenuOut)
      : false;
    });
    $(window).on("scroll", () => {
      displayIntro();
      selectMenu(false);
    });
    $(document).on("keydown", (e) => {
      if ( e.key === "Escape" ) {
        friseModal(el=undefined, closeOnly=true);
        $(".burger").removeClass("burger-cross");
        $(".page-container").removeClass("sidebar-active");
      }
    });
    $(".map-container").on("mouseenter", () => {
      $(".map-container").find(".leaflet-interactive[stroke='var(--cs-plum)']")
                         .animate({ opacity: 0.5 }, 500);
    });
    $(".map-container").on("mouseleave", () => {
      $(".map-container").find(".leaflet-interactive[stroke='var(--cs-plum)']")
                         .animate({ opacity: 1 }, 500);
    });
    $(".frise div").on("click touchend", (e) => { 
      e.type === "click" ? friseModal( $(e.currentTarget) ) : false;
    });
    $(".burger").on("click touchend", (e) => {
      e.type === "click" ? toggleSidebar() : false;
    });
    $(".menu a, .menu-items a").on("click touchend", (e) => { 
      e.type === "click" ? scrollMenu(e) : false;
    });
    $(".more > button").on("click touchend", (e) => {
      e.type === "click" ? displayTextFull(e.currentTarget) : false;
    }) 
    $("#partenaires > .content-title-container:last-child > h1").on("click touchend", (e) => {
      e.type === "click"
      ? creditsLegalModal($(`#${$(e.currentTarget).attr("title")}`))
      : false;
    });
    $(".toggler").on("click touchend", async (e) => {
      /** 
       * switching OPENSEADRAGON / LEAFLET viewers 
       *
       * 4 modes are defined. those modes allow for library specific
       * behaviour in our functions 
       * - `osd` => openseadragon 
       * - `lfl` => leaflet. 
       * - `pip` => pipeline
       * - `d3` => 3d model
       */
      const clicked = $(e.currentTarget);
      const click = await clickOrScroll(clicked);
      if ( e.type === "click" && click ) {
        // leaflet map
        if ( 
          clicked.parent().is("#cartographie .map-container")
          && !globals.currentToggling
        ) {
          switchViewer(clicked, "lfl");
        
        // icono osd viewer
        } else if ( 
          !globals.currentToggling 
          && clicked.parent().is( $("#iconographie .osd-container") ) 
        ) {
          switchViewer(clicked, "icn");
          
        // pipeline osd viewer
        } else if ( 
          !globals.currentToggling 
          && clicked.parent().is( $("#pipeline .osd-container") ) 
        ) {
          switchViewer(clicked, "pip");
          
        } else if (
          !globals.currentToggling
          && clicked.parent().is("#modele3d .osd-container")
        ) {
          switchViewer(clicked, "d3");
        }
      }
    });
    
    
    /************** DOM MANIPULATION **************/
    async function clickOrScroll(el) {
      /**
       * for mobile devices: check if we have clicked or scrolled:
       * if in 0.1s, the page has moved up or down by more than 5px.
       *
       * to do so we must use a promise with a `setTimeout()` which returns
       * a value using the `resolve()` function. if there's an error, it's the
       * `reject()` which is fired. 
       * see: https://stackoverflow.com/questions/24928846/get-return-value-from-settimeout
       * 
       * @param {jquery html element} el: the element from which to get the offset top
       * @return {bool}: true if click (scroll <= 5 px), false if scroll (scroll > 5px)
       */
      if ( 
        globals.burger.css("display") !== "none" 
        && globals.currentToggling === false 
      ) {
        const start = el.offset().top;
        return new Promise((resolve, reject) => {
          setTimeout( () => {
            resolve( Math.abs( start - el.offset().top ) <= 5 );
            reject(false);
          }, 100 )
        });
      } else {
        return true;
      }
    }
    
    
    function createMenu() {
      /**
       * create the menu (inside `.menu`) from the html 
       * ids of each `.content` element
       */
      const contentBlocks = $(".content");
      for ( let i=0; i < contentBlocks.length; i++ ) {
        $("<a></a>").attr( "href", `#${$(contentBlocks[i]).attr("id")}` )
                    .text( $(contentBlocks[i]).find("h1").text() )
                    .appendTo($("<li></li>"))
                    .parent()
                    .appendTo($(".menu ul"));
      }
      globals.menuLinks = $(".menu a");
    }


    function scrollMenu(e) {
      /**
       * smooth scrolling to page elements from `.menu`
       * the weird regex allors to keep only the html id 
       * from the href.
       *
       * @param {html event} e: the clicked event.
       */
      e.preventDefault();
      $("html").animate({
        scrollTop: $( e.target.href.replace(/^[^#]+/g, "") ).offset().top - $(".navbar").height()
      }, globals.animDur);
      setTimeout( () => { 
        if ( ! $(".sidebar-container").hasClass("sidebar-hide") ) { toggleSidebar() }
      }, 350 );  // close the sidebar
      return
    }

    
    async function initTextBlocks() {
      /**
       * on page load, add the inner html of all 
       * `.text-block` elements: display the text
       * for the current viewer.
       */
      animateTextBlockHeight( $("#cartographie").find(".text-block")
                              , globals.lflIds[0]
                              , true );
      animateTextBlockHeight( $("#iconographie").find(".text-block")
                              , globals.icnIds[0]
                              , true );
      animateTextBlockHeight( $("#pipeline").find(".text-block")
                              , globals.pipIds[0]
                              , true );  
    }
    
    
    async function displayIntro() {
      /**
       * display/hide the whole intro view
       */
      // hide
      if ( $(window).scrollTop() > 0.2 * $(window).height() ) {
        $(".intro").css({ transform: "translate(0, -100%) rotateX(90deg)" }) 
      // display
      } else {
        $(".intro").css({ transform: "translate(0, 0) rotateX(0deg)" });
      }
    }  
  
  
    function introMenu(f) {
      /**
       * display/hide the intro menu. originally run inside an interval,
       * which causes 2 issues:
       * - the interval often keeps on running forever
       * - the interval goes in panic and show/hide the menu without
       *   any activity, or shows the menu when it should hide it.
       * @param {function} f: the function to run
       */
     // const run = (int) => { f(); clearInterval(int) };
     // const interval = setInterval(() => {   
        const cond = f.name === "introMenuIn"
                     ? $(".intro-menu-container .menu-container").height() === 0
                     : $(".intro-menu-container .menu-container").height()
                       >= 0.55 * $(".intro-menu-container").height();  // 60% of the parent container => the menu is displayed and visible
        console.log(f.name);
        if ( cond && ! globals.currentIntroToggle ) {
          f()// run(interval);
        }
      // }, 500)
    };
        
    
    function introMenuIn() {
      /**
       * activate the `.intro .intro-menu-container` full display.
       * takes ~500ms to complete.
       */
      console.log("in");
      globals.currentIntroToggle = true;
      $(".intro-menu-container").addClass("rotated").children(".svg-container").animate(
        { "margin-top": "60vh" }
        , 500
        , () => { 
          $(".intro-menu-container").addClass("menu-display");
          $(".intro-menu-container > .menu-container").addClass("display");
          $(".intro-menu-container > .svg-container").css({ "margin-top": "0" });
          setTimeout( () => {globals.currentIntroToggle  = false}, 500 );
        }
      )
    }
    
    
    function introMenuOut() {
      /**
       * deactivate the `.intro .intro-menu-container` full display.
       * takes ~500ms to complete.
       */
      console.log("out");
      globals.currentIntroToggle = true; 
      $(".intro-menu-container").removeClass("menu-display")
      $(".intro-menu-container > .menu-container").removeClass("display")
      $(".intro-menu-container > .svg-container").css({ "margin-top": "60vh" })
                                                 .animate(
                                                   { "margin-top": 0 }
                                                   , 500
                                                   , () => { globals.currentIntroToggle = false }
                                                 );
      setTimeout( () => {$(".intro-menu-container").removeClass("rotated")}, 200);
    }
    
    
    function displayTextFull(c) {
      /**
       * display the extra text within a `.content .text-block .text-full`
       * when clicking on the "En savoir +" (`.content .more button`)
       *
       * @param {jQuery} c: the clicked element
       */
      // define variables
      const moreBlock = $(c).parent();  // `.more` containing the `.clicked`
      const contentBlock = moreBlock.parent();
      let currentId = "";
      switch ( contentBlock.attr("id") ) {
        case "cartographie":
          currentId = globals.currentLflId;
          break;
        case "iconographie":
          currentId = globals.currentIcnId;
          break;
        case "pipeline": 
          currentId = globals.currentPipId;
          break;
      };
      const textFull = contentBlock.find(`.text-block div${ currentId !== "" ? "[title='" + currentId + "']" : "" } .text-full`)
      
      // show the `.text-full`
      if ( ! moreBlock.hasClass("less") ) {
        showEl(textFull);
        contentBlock.children(".text-block").css({ height: "100%" });
        moreBlock.addClass("less").find("p").text("Voir moins");
        
      // hide the `.text-full`
      } else {
        hideEl(textFull);
        contentBlock.children(".text-block").css({ height: "100%" });
        moreBlock.removeClass("less").find("p").text("En savoir");
        
      }
    }
    
    async function switchViewer(clicked, mode) {
      /**
       * switch the viewer when clicking the `.toggler` buttons
       *
       * global logic:
       * ************* 
       * - html ids help to target the openseadragon/leaflet map to display
       * - when clicking on a viewer, `switchViewer()`: performs the changes
       *   - hide the text elements related to the old view
       *   - define the new html id of the viewer to target
       *   - create the new viewer
       *   - await for the above functions to return
       *   - end the viewer switching animations: fade the new text and legend in.
       * - during the whole time, a `globals.currentToggling` variable logs
       *   that an animation is ongoing => this avoids mutliple animations
       *   to occur at the same time
       *
       * where do we get our texts from?
       * *******************************
       * - the text for all viewers in `.text-block` is present in the
       *   html pages with `display: hidden`. only the text with an `@title`
       *   of the same value as the `@id` of the current viewer is visible
       * - the text for all legends (`.viewer-legend`) is contained in json
       *   files which are sourced asynchronously
       *
       * globals.currentToggling
       * ***********************
       * we use a state variable `globals.currentToggling`
       * to disable the switching of leaflet map / openseadragon
       * viewer. this avoids conflict between different occurences 
       * of `switchViewer()`, `defineLfl()` & `defineOsd()` which cause 
       * errors
       * - map/osd toggling is enabled when a button is clicked and
       *   `globals.currentToggling===false`: no map/osd toggling 
       *   is currently happening
       * - `globals.currentToggling` is then switched to `true`
       *   until the toggling has completed.
       *
       * @param {jQuery} clicked: the clicked element
       * @param {str} mode: the mode / type of viewer we're working on. possible values: 
       *                    - `icn`: Openseadragon icono viewer 
       *                    - `pip`: openseadragon pipeline
       *                    - `lfl`: Leaflet* 
       * @returns {none}
       */
      /********************* LOAD DATA *********************/
      globals.currentToggling = true;
      var idOld, indexes, contentBlock;
      switch ( mode ) {
        case "icn":
          idOld = globals.currentIcnId;
          indexes = globals.icnIds;
          contentBlock = $("#iconographie");
          break;
        case "pip":
          idOld = globals.currentPipId;
          indexes = globals.pipIds;
          contentBlock = $("#pipeline");
          break;
        case "lfl":
          idOld = globals.currentLflId;
          indexes = globals.lflIds;
          contentBlock = $("#cartographie");
          break;
        case "d3":
          idOld = globals.currentD3Id;
          indexes = globals.d3Ids;
          contentBlock = $("#modele3d");
          break;
      }
      
      /********************* START ANIMATION *********************/
      // hide necessary elements from the previous view
      const legendHeight = contentBlock.find(".viewer-legend").height();
      if ( mode !== "d3" ) {
        const textHeight = contentBlock.find(".text-block").height() + contentBlock.find(".more").height();
        hideMore(contentBlock);
        await hideEl(
          contentBlock.find(`.text-block div[title="${idOld}"]`)
          , () => {
            contentBlock.find(`.text-block`)
                        .css({ height: textHeight });
          }
        );
        contentBlock.find(`.text-block`).append($(
          `<div class="loader-container" style="display: flex">
            <span class="loader"/>
          </div>`
        ));
        contentBlock.find(".text-full").addClass("hide");
      };
      hideEl(
        contentBlock.find(".viewer-legend div")
        , () => { contentBlock.find(".viewer-legend").css({ height: legendHeight }) }
      );
      
      /********************* UPDATE VIEWER *********************/
      // get the index of the new item to display in `indexes`
      let idx = indexes.indexOf( idOld );  // index of the viewer's id in `globals.(lflIds|icnIds)`
      if ( clicked.is(":first-child") ) {
        idx > 0 ? idx-- : idx = indexes.length-1;
      } else {
        idx+1 === indexes.length ? idx = 0: idx++;
      }
      const idNew = indexes[idx];
      
      // remove the old viewer + log the current id in `globals` 
      // + update the `id` of the viewer to select the new viewer
      switch ( mode ) {
        case "lfl":
          globals.lflViewer.off();
          globals.lflViewer.remove();
          globals.lflViewer = null;
          globals.currentLflId = globals.lflIds[idx];
          break;
        case "icn":
          globals.icnViewer.destroy();
          globals.icnViewer = null;
          globals.currentIcnId = globals.icnIds[idx];
          break;
        case "pip":
          globals.pipViewer.destroy();
          globals.pipViewer = null;
          globals.currentPipId = globals.pipIds[idx];
          break;
        case "d3": 
          globals.d3Viewer.destroy();
          globals.d3Viewer = null;
          globals.currentD3Id = globals.d3Ids[idx];
      }
      contentBlock.find(`#${idOld}`).attr("id", idNew);  // update the id of the viewer
      
      // update the viewer
      switch ( mode ) {
        case "icn":
          await defineOsd("icn");
          break;
        case "pip":
          await defineOsd("pip");
          break;
        case "d3":
          await defineOsd("d3");
          break;
        case "lfl":
          await defineLfl();
          break;
      }
      
      /********************* END ANIMATION *********************/
      
      // update the legend + text + `.more` button
      setTimeout( async () => {
        if ( mode !== "d3" ) {
          hideEl(
            $(".loader")
            , () => { 
              // remove the loader
              $(".loader-container").remove();
              // add the new text
              animateTextBlockHeight(contentBlock.find(".text-block"), idNew);
              // update the `.more` button by adding it back if necessary
              contentBlock.has(`.text-block div[title=${idNew}] .text-full`).length
              ? showMore(contentBlock) 
              : false;
              // update the legend
              showEl(
                contentBlock.find(`.viewer-legend div[title="${idNew}"]`)
                , () => {               
                  contentBlock.find(".viewer-legend").css({ height: "100%" });
                  globals.currentToggling = !globals.currentToggling; 
                } 
              );
              $("html").animate({ 
                scrollTop: contentBlock.offset().top - $(".navbar").height() 
              }, globals.animDur);
            }
          );
        }
        else {
          // update the legend
          showEl(
            contentBlock.find(`.viewer-legend div[title="${idNew}"]`)
            , () => { 
              contentBlock.find(".viewer-legend").css({ height: "100%" });
              globals.currentToggling = !globals.currentToggling; 
            } 
          );
          $("html").animate({ 
            scrollTop: contentBlock.offset().top - $(".navbar").height() 
          }, globals.animDur);
        }
      }, 500);
      return;
    }
    
    
    function animateTextBlockHeight(textBlock, textTitle) {
      /**
       * animate the updating of the text 
       * + changing of height of `.text-block` elements
       *
       * see: https://stackoverflow.com/questions/5003220/animate-element-to-auto-height-with-jquery
       *
       * @param {jQuery} textBlock: the html `.text-block` element to update
       * @param {str} textTitle: `@title` of the `.text-block > div` to display, which
       *                         matches the html id of the current viewer
       */
      showEl(textBlock.find(`div[title="${textTitle}"]`));
      textBlockHeight = textBlock.height();
      textBlock.css({ height: "auto" });
      const autoHeight = textBlock.height();
      textBlock.height( textBlockHeight )
               .animate(
                 { height: autoHeight }
                 , globals.animDur
               );
      return
    }
    
    
    function friseModal(el=undefined, closeOnly=false) {
      /**
       * open/close a modal window containing an image in `.frise`.
       * if a `.modal` aldready exists, we close it. else, we open 
       * the clicked `.frise div` inside a modal. we animate opening and
       * closing in a somewhat complex way
       *
       * opening
       * *******
       * all items in `.frise` are `<div><img></div>`.
       * we just add a class `modal` to the clicked `.frise div` 
       * to make it into a modal window. the clicked `.frise div` is 
       * removed from `.frise`, so we log its position within `.frise` 
       * in the `sessionStorage` to be able to reinsert it in the proper 
       * positon later on
       *
       * closing
       * *******
       * we get the position of the element stored in the session storage,
       * reposition it at that position and remove the position from the
       * storage.
       * 
       * @param {jQuery} el: the clicked div in `.frise`, as a jquery element.
       *                     optionnal since unused when closing
       * @param {bool} closeOnly: only enable closing the modal and 
       *                          disable the opening code block. avoids errors.
       */
      // OPENING
      if ( $(".modal").length == 0 && !closeOnly ) {
        // log the position of the clicked div in `.frise` in a the `sessionStorage`
        window.sessionStorage.setItem( "friseModalPosition", $(".frise > *").index(el) );
        // open the image in a modal (which moves it from `.frise` to `.content-container`)
        openModal(el);
        
      // CLOSING
      } else {
        const el = $(".modal");
        const idx = Number(window.sessionStorage.getItem("friseModalPosition"))-1;
        closeModal(el).then((e) => { 
          console.log(e);
          idx >= 0 
          ? el.insertAfter($( $(".frise div")[idx] ))  // not the first item => append at position
          : el.prependTo($(".frise"));                 // first item => append at position 0  
        })
        window.sessionStorage.removeItem("friseModalPosition");
      };
      return;
    }
    
    async function creditsLegalModal(el) {
      /**
       * display/hide a window containing `#credits` or `#legal`.
       * @param {jQuery}: `$(#credits)` or `$(#legal)`
       * @returns: none
       */
      if ( ! ( el.parent().hasClass("modal") ) ) {
        openModal(el, container=true).then(() => { 
          el.removeClass("hide")
            .css({ opacity: 1, marginBottom: 0 }) 
            .parent()
            .on("click touchend", (e) => { 
              // listen to a closing event listener. has to be created here for some reason
              if ( 
                e.type === "click" 
                && ! $(e.target).closest(el).length  // only close if we click outside of the displayed el
              ) {
                closeModal($(".modal"), container=true).then((_el) => {
                  _el.addClass("hide")
                     .css({ marginBottom: "auto" }) 
                     .insertAfter($( $(".content")[$("#partenaires").index()] ));
                });
              };
          });
        });
      }
    }
    
    
    /************** DOM HELPERS **************/
    async function openModal(el, container=false) {
      /**
       * open a modal window
       * @param {jQuery} el: the element displayed in a modal
       * @param {bool} container: flag to enclose `el` inside a container (for css purposes)
       * @returns {jQuery} el: the same element, with the modal container (created if 
                               `container===true`; if `container===false`, then el 
                               aldready contains a modal container)
       */
      const shared = (e) => {
        // shared operations in both cases.
        // returns the child of `e`, so it zaps out the modal container
        return e.addClass("modal")
                .prependTo("body")
                .children()
                .css({ opacity: 0 })       
      }
      if ( container ) {
        el = $("<div></div>").addClass("modal").append(el);
        console.log(shared(el));
        return shared(el).children()
                         .animate({ opacity: 1 }, globals.animDur)
                         .parent()
                         .promise();
      } else {
        return shared(el).animate({ opacity: 1 }, globals.animDur)
                         .parent()
                         .promise();
      }
    }
    
    async function closeModal(el, container=false) {
      /**
       * close a modal window.
       * @param {jQuery} el: the `.modal` to hide
       * @param {bool} container: flag to remove the container containing `el` (for css purposes)
       * @returns {jQuery} el: if `container===true`, the element inside the `.modal`.
       *                       else, the `.modal` element and its contents
       */
      return el.children().animate(
          { opacity: 0 }
          , globals.animDur / 3
          , () => {
            if ( container ) {
              return el.removeClass("modal").children().css({ opacity: 1 });
            } else {
              return el.removeClass("modal").children().css({ opacity: 1 }).parent();
            };
        } 
      ).promise();
    }
  
    async function hideEl(el, callback=undefined) {
      /**
       * animate the hiding of an element with `.hide`.
       * using `async` allows to wait for the change of `el`
       * @param {jQuery} el: the element to hide
       * @param {func} callback: the callback function to execute at the end
       * @returns {jQuery}: the element
       */
      return el.animate({ opacity: 0 }, globals.animDur / 2, () => { 
        el.addClass("hide"); 
        callback !== undefined ? callback() : false; 
        return el; 
      }).promise();
    }
    
    async function showEl(el, callback=undefined) {
      /**
       * animate the showing of an element when removing `.hide`.
       * using `async` allows to wait for the change of `el`
       * @param {jQuery} el: the element to show
       * @param {func} callback: the callback function to execute at the end
       * @returns {jQuery}: the element
       */
      return el.removeClass("hide").animate({ opacity: 1 }, globals.animDur, () => { 
        callback !== undefined ? callback(): false; 
        return el; 
      }).promise();
    }
    
    async function hideMore(contentBlock) {
      /**
       * hide the `.more` button
       * @param {jQuery} contentBlock: the $(".content") element in which to hide the `.more`
       * @returns {none}
       */
      hideEl(contentBlock.children(".more"));
    }
    
    function showMore(contentBlock) {
      /**
       * display the `.more` button
       * @param {jQuery} contentBlock: the $(".content") element in which to hide the `.more`
       * @returns {none}
       */
      showEl(
        contentBlock.children(".more")
        , () => { contentBlock.children(".more").removeClass("less").find("p").text("En savoir") }
      );
    }

    
    /************** MAP + LEAFLET FUNCTIONS **************/
    async function defineLfl() {
      /**
       * define all the maps we will use.
       * this is the main function containing the logic for all maps
       */
      const target = $(".map");
      const mapId = target.attr("id");  // the type of map to display
      const richelieuZone = await loadStaticsFile("data/zone.geojson");
      
      // create the map
      globals.lflViewer = L.map(mapId, {
        center: [ 48.8687452, 2.3363674 ]
        , maxBounds: L.latLngBounds([
          { lat: 48.8856701621242, lng: 2.3092982353700506 }
          , { lat: 48.829997780023035, lng: 2.3845843750075915 }
        ])
        , inertia: false  // inertia does weird things with the geojson layer
        , maxBoundsViscosity: 1.0
        , scrollWheelZoom: false
        , zoomControl: false
        , minZoom: 13  // 11 for paris + region, 13 for the neighbourhood 
        , zoom: 14.7
      });
      L.control.zoom({
        position: "topright"
      }).addTo(globals.lflViewer);
      
      // define the panes (groups of layers defined by their `z-index`)
      globals.lflViewer.createPane("basePane");  // elements at the base, non-deletable.
      globals.lflViewer.getPane("basePane").style.zIndex = "0";
      globals.lflViewer.createPane("dataPane");  // pane on which we'll add data
      globals.lflViewer.getPane("dataPane").style.zIndex = "1";
      
      // define background map tiles
      const backgroundTiles = buildBackgroundTiles(richelieuZone);
      
      // bg tiles that can't be disabled: stamen tiles + geoJson borders
      backgroundTiles[0].addTo(globals.lflViewer);
      backgroundTiles.findLast((layer) => { layer.addTo(globals.lflViewer) });
          
      // display slide-specific data
      if ( mapId !== "map-base" ) {
        slideSpecificLayer = await buildSlideSpecificLayer(mapId);
        slideSpecificLayer.addTo(globals.lflViewer);
      }
      
      // build layer groups to enable / disable certain tiles and popups
      L.control.layers(
        {}, undefined, { hideSingleBase: true, position: "bottomright" }
      )// .addOverlay(backgroundTiles[1], "Carte de l'etat-major 1&nbsp;: 40 000 (1820-1866)")
       // .addOverlay(backgroundTiles[2], "Carte de Paris (1906)")
       .addOverlay(backgroundTiles[2], "Atlas 1900")
       .addOverlay(backgroundTiles[1], "Carte Alpage-Vasserot")       
       .addTo(globals.lflViewer);
    
      // once all data is loaded, 
      // - re-center the map 
      // - change the map view once a popup has loaded 
      //   so that the popup can be visible
      let lng = [];
      let lat = [];
      richelieuZone.features[0].geometry.coordinates[0][1].map(
        lnglat => lng.push(lnglat[0]) && lat.push(lnglat[1])
      );
      globals.lflViewer.fitBounds([
        [ Math.min(...lat), Math.min(...lng) ]
        , [ Math.max (...lat), Math.max(...lng) ]
      ]);
      popupVisible();
      
      // async return <div className="div that can resize""
      globals.lflViewer.on("load", () => {
        return;
      })
    };
    
    
    function popupVisible() {
      /**
       * make a popup entierly visible by waiting
       * for the image inside it to load and recalculating
       * the popup height afterwards
       *
       * see: 
       * - https://github.com/Leaflet/Leaflet/issues/5484
       * - https://stackoverflow.com/questions/51732698/leaflet-popup-update-resizing-solution-recreating-a-popup-everytime-unable/51749619#51749619
       */
      document.querySelector(".leaflet-popup-pane")
              .addEventListener("load", (e) => {
        const tagName = e.target.tagName;
        const popup = globals.lflViewer._popup;
        
        if ( tagName === "IMG" && popup && !popup._updated ) {
          popup._updated = true;          
          popup.update();
        }      
      }, true);
    }
    
    
    async function buildSlideSpecificLayer(mapId) {
      /**
       * build the geoJson layer containing data
       * that is specific to a single slide in the 
       * map carrousel.
       *
       * @param {string} mapId: the current map's html id, to define
       *                        the data that needs to be displayed
       * @returns {L.geoJSON}: the L.geoJSON layer containing slide-specific data
       */
      // load the data to display from a server 
      const slideData = await loadStaticsFile(`data/${mapId}.geojson`);
      
      // build the geoJSON layer
      return L.geoJSON(
        slideData, {
          style: (f) => {
            // define inidivual styles
            return {
              "color": f.properties.fill,
              "fillOpacity": 100
            }
          },
          onEachFeature: (f, layer) => {
            // define popups
            if ( Object.keys(f.properties).includes("ressourceId") 
                 && f.properties.ressourceId !== "" ) {
              // add images to the popup + add data about the image
              const id_ = f.properties.ressourceId;
              if ( globals.icnIds.includes(id_) ) {
                // the above condition should be deleted since 
                // all features should have an image
                const legend = $(`.viewer-legend div[title="${id_}"] p`);
                const style = $(".burger").css("display") !== "none" 
                              ? `width: 100%; 
                                 display: flex; 
                                 flex-direction: column;
                                 justify-content: center;`
                              : "auto"
                layer.bindPopup(
                  `<div style="${style}">
                     <div style="width: 65vw;">
                       <img src="${globals.icnData[id_]["img_url"]}"
                            alt="Reproduction de: ${legend.text()}"
                            style="max-width: 100%"/>
                       ${legend[0].outerHTML}
                     </div>
                   </div>`
                  , { 
                    maxHeight: 0.5 * $(".map")[0].scrollHeight, 
                    maxWidth: $(document).width() < 900 ? "75vw !important" : "auto" 
                  }
                )
              }
            } else {
              // simple popup with only `name`
              layer.bindPopup(
                `<div>
                   <p>${f.properties.name}</p>
                 </div>`
              );
            }
          }
        }
      )
    }
  
    
    /************** IIIF AND OPENSEADRAGON FUNCTIONS **************/
    async function defineOsd(mode) {
      /**
       * define an Openseadragon viewer based on the html id
       * of `.osd` and of mode. this html id points to a key of 
       * `globals.icnData`|`globals.pipData`
       *
       * @param {string} mode: one of "intro"|"main"|"pipeline". to indicate 
       *                       the viewer to select and the images to display.
       *                       - `intro`: the non togglable viewer in the intro
       *                       - `icn`: the main openseadragon togglable viewer
       *                       - `pip`: the pipeline viewer at the end
       */
      // target the proper viewer. then, extract the id is
      // a key of `globals.icnData`
      switch ( mode ) {
        case "icn": 
          var target = $("#iconographie .osd-main");  // html element to target
          var osdId = target.attr("id");              // identifier of the image to display
          var tileSequence = await processManifest(globals.icnData[osdId].manifest_url);
          var viewer = buildOsdViewer( tileSequence, osdId );
          globals.icnViewer = viewer
          break;
        case "pip":   // in that case, dzi filenames do not totally match osdId: `-` is used as separator in place of `_` 
          var target = $("#pipeline .osd-main");
          var osdId = target.attr("id");
          var viewer = buildOsdViewer( `${globals.serverUrl}statics/img/pipeline/${osdId}.dzi`, osdId );
          globals.pipViewer = viewer;
          break;
        case "d3":
          var target = $("#modele3d .osd-main");
          var osdId = target.attr("id");
          var viewer = buildOsdViewer( `${globals.serverUrl}statics/img/3d/${osdId}.dzi`, osdId );
          globals.d3Viewer = viewer;
          break;
        /*
        case "intro":
          var target = $(".osd-intro");
          var osdId = globals.introIcnUuid
          var tileSequence = await processManifest(globals.icnData[osdId].manifest_url);
          var viewer = buildOsdViewer( tileSequence, "intro-osd-viewer" );
          break;
        */
      }
      
      // handlers: open failed + allow normal scroll
      viewer.addOnceHandler( "open-failed", (e) => osdOpenFailed(e, tileSequence) );
      viewer.addHandler("canvas-scroll", (e) => { e.preventDefault = false; });
      
      // async return 
      viewer.addOnceHandler("open", () => { return });
    }
    
    
    function buildOsdViewer(tileSequence, osdId) {
      /**
       * build an openseadragon viewer using `tileSequence`
       *
       * @param {Array} tileSequence: the tiles to display in the viewer
       * @param {string} osdId: the openseadragon id of the tile
       */
      return OpenSeadragon({
        id: osdId,
        prefixUrl: `${globals.serverUrl}statics/img/openseadragon-icons/`,
        tileSources: tileSequence,
        sequenceMode: true,
        initialPage: 0,
        showHomeControl: true,
        autoHideControls: true,
        crossOriginPolicy: true,
        viewportMargins: {
          top: 10,
          left: 10,
          right: 10,
          bottom: 10
        },
        gestureSettingsMouse: { scrollToZoom: false },
        showSequenceControl: typeof(tileSequence) === Array 
                             ? tileSequence.length > 1      // if we provide an array of IIIF images, sequenceControl will depend on the number of images
                             : false,                       // if we provide a dzi url, don't show sequenceControl buttons.
        showNavigator: true,
        navigatorAutoFade: true,
        showRotationControl: true,
        navImages: globals.navImages
      });
    }
    
  
    function osdOpenFailed(e, tileSequence) {
      /**
       * function to handle `open-failed` osd event on 
       * a viewer meaning that a tile source from 
       * `tileSequence` can't be opened. 
       * - remove the current viewer
       * - remove faulty tile sources from `tileSequence`
       * - create a new openseadragon viewer with the new
       *   tileSequence
       *
       * this function is recursive: if there's a new 
       * `open-failed` event, the function will be run 
       * again, until `tileSequence` is empty. then, an
       * error message will be displayed.
       *
       * @param {html event} e: the event object
       * @param {Array} tileSequence: the array of tile sources
       */
      const origTsLen = tileSequence.length;
      const osdId = `#${$(e.eventSource).attr("id")}`;
        
      if ( origTsLen >= 1 ) {
        $(osdId).html("");
        // remove the faulty tile source from tilesequence
        for ( let i=0; i < origTsLen; i++ ) {
          var idx = -1;
          if ( tileSequence[i].url === e.options.tileSource.url ) {
            idx = i
            break
          }
        };
        // delete the faulty tileSource from `tileSequence`.
        ( tileSequence.length >= 1 && idx >= 0 )
        ? tileSequence.splice(idx, 1) 
        : tileSequence;

        if ( tileSequence.length >= 1 && tileSequence.length !== origTsLen ) {
          // if `tileSequence` isn't empty, create a new viewer + add recursive handler
          globals.icnViewer = buildOsdViewer(tileSequence, e.eventSource.id);    
          globals.icnViewer.addOnceHandler( "open-failed", (e) => osdOpenFailed(e, tileSequence) );
        } else {
          // else, display an error message
          $(osdId).html("<p>Échec du chargement de l'image</p>")
                   .css({
                     "background-color": "#000000",
                     "color": "var(--cs-gold)",
                     "font-size": "150%",
                     "display": "flex",
                     "align-items": "center",
                     "justify-content": "center"
                   });
        }
      }
    }
    
  })
  /* END `GLOBALS` BLOCK */
  
  
  /************** AUXILIARY / UTILS FUNCTIONS **************/
  // functions here don't need to access the variables in `globals`
  
  /* DOM MANIPULATION */
  function createMenu() {
    /**
     * create the menus (inside `.menu` and `.intro-menu-container`) 
     * from the html ids of each `.content` element
     */
    // sidebar menu
    const contentBlocks = $(".content:not(#legal):not(#credits)");
    for ( let i=0; i < contentBlocks.length; i++ ) {
      $("<a></a>").attr( "href", `#${$(contentBlocks[i]).attr("id")}` )
          .text(() => {
            txt = $(contentBlocks[i]).find("h1:first").text()
            if ( txt === "RICH.DATA" ) { txt = "Rich.Data"; }
            return txt;
           })
          .appendTo($("<li></li>"))
          .parent()
          .appendTo($(".menu ul"));
    }
    
    // intro menu
    let links = [ "Le projet", "Activités", "Explorer", "Restituer", "Rich.Data" ];
    for ( let i=0; i < contentBlocks.length; i++ ) {
      links[i] = $(`
        <a href="#${ $(contentBlocks[i]).attr("id") }">
          ${ links[i] }
        </a>
      `);
    }
    for ( let i=0; i < $(".menu-items > div").length; i++ ) {
      $($(".menu-items > div")[i]).append(links[i]);
    };
  }


  function selectMenu() {
    /**
     * select the currently active element in `.menu` based on
     * the positionning of `.content` elements relative to the viewport.
     * the `.content` element to highlight in the menu is the one with 
     * the closest absolute distance between its top and the viewport'
     * top.
     *
     * - define `matchedId` ; the html id of the matched `.content` 
     *   with the smallest absolute distance to the viewer's top
     *   by default, `matchedId` is the first element.
     * - target the corresponding `<a>` amongst the  
     *   `$(".menu li")`: the href of this element matches
     *   `#${matchedId}`.
     */
    const contentBlocks = $(".content:not(#credits):not(#legal)");
    let matchedId = $($(".content")[0]).attr("id");  // by default, the html id of the 1st content container.
    // find the `.content` with the smallest distance to viewport 
    // top by building a dict of distances to the viewport top
    let distances = {};
    contentBlocks.map( (i) => {
      distances[ Math.abs(contentBlocks[i].getBoundingClientRect().y) ] 
      = $(contentBlocks[i]).attr("id")
    });
    matchedId = distances[ Math.min(...Object.keys(distances)) ];
    
    $(".menu li").removeClass("selected");
    $(`.menu a[href="#${matchedId}"]`).parent().addClass("selected");
    return
  }
   
    
  function toggleSidebar() {
    /** 
     * toggle the visibility of `sidebar`
     */
    const burger = $(".burger");
    const pageContainer = $(".page-container");
    
    // only activate the function on small viewports
    if ( burger.css("display") !== "none" ) {
      // deactivate
      if ( burger.attr("class").split(" ").includes("burger-cross") ) {
        burger.removeClass("burger-cross")
        pageContainer.removeClass("sidebar-active");
      // activate
      } else {
        burger.addClass("burger-cross");
        pageContainer.addClass("sidebar-active");
      }
    }
    return
  }
  
  function buildIntro() {
    /**
     * build the design in `.intro`: a map that, when hovered,
     * reveals a menu, where each menu item is a map location
     */
    // x position for all `.menu-container div`s
    const xPos = [ 0.6, 0.8, 0.5, 0.3, 0.1 ];
    const parPos = [ 
      document.querySelector(".intro-menu-container")
              .getBoundingClientRect()
              .left
      , document.querySelector(".intro-menu-container")
                .getBoundingClientRect()
                .top 
    ]
    
    // center point of all our circles. array of `[x,y]` arrays
    let cirPos = [];
    for ( let i=0; i < $(".intro-menu-container circle").length; i++ ) {
      let rect = $(".intro-menu-container circle")[i].getBoundingClientRect();
      rotated = rotationCalculator([[
        rect.x  + rect.width / 2 - parPos[0]    // x position
        , rect.y + rect.height / 2 - parPos[1]  // y position
        , 0                                     // z is assumed to be 0, since a screen is in 2d
      ]])[0]
      cirPos.push([ 
        rotated[0] - parPos[0]
        , rotated[1] + 0.63 * $(".intro-menu-container").height()  // a +60vh block that will be added on top. smh with 63% we get the good result
      ]);
    }
    cirPos.sort((a,b) => { 
      // sort `cirPos` items by their x position, ascending
      if ( a[0] == b[0] ) {  return 0; }
      return ( a[0] > b[0] ? 1 : -1 );
    });
    
    /************* build our items *************/
    for ( let i=0; i < $(".menu-items > div").length; i++ ) {
      let rect = $(".menu-items > div")[i].getBoundingClientRect();
      
      // position the menu items
      $($(".menu-items > div")[i]).css({ 
        left: `${ cirPos[i][0] - rect.width }px`,
        top: `${ xPos[i] * 0.6 * $(".intro-menu-container").height() }px`  // position==absolute, so percent values are relative to the window => convert them so they are relative to the parent
      });
      rect = $(".menu-items > div")[i].getBoundingClientRect();  // update rect now that the item is positionned.
      
      // draw a line between those and the map circles
      $($(`.menu-pointers div`)[i]).css({
        width: "2px",
        top: `${ rect.top - parPos[1] }px`,
        left: `${ rect.right - parPos[0] - 1 }px`,
        height: `${ cirPos[i][1] - (rect.top - parPos[1]) }px`  // distance between `top` and circle position
      });
      
    }
  }
  
  
  function rotationCalculator(pt, deg=70) {
    /**
     * calculate the position of a coordinate after 
     * applying a rotation to it with `rotateX`
     * using 
     * - the matrix in the MDN docs: https://developer.mozilla.org/en-US/docs/Web/CSS/transform-function/rotateX#values
     * - this wikipedia article (see Basic rotations > Rx): https://en.wikipedia.org/wiki/Rotation_matrix
     *
     * @param {Array} pt: the point, an array of `[x,y,z]` position
     * @param {Number} deg: the rotation degree
     * @returns {Array}: the [x,y] position after rotation
     */
    deg = deg * Math.PI / 180;  // convert degrees to radiant
    return matrixer(
      // point matrix
      pt,
      // rotation matrix for Rz
      [ 
        [ 1, 0, 0 ], 
        [ 0, Math.cos(deg), -Math.sin(deg) ], 
        [ 0, Math.sin(deg), Math.cos(deg) ] 
      ]
      
    )
  }
  
  
  function matrixer(a, b) {
    /**
     * multiply 2 matrixes.
     *
     * all matrixes are arrays of arrays of numbers: [][]
     * the size of the matrix is expressed as (rows * cols).
     * a matrix can be multiplied if A is of size (n*m) and B 
     * of size (m*p)
     *     +- column
     *     |
     * [   v
     *   [ 1, 1, 1 ], <- row
     *   [ 1, 1, 1 ],
     *   [ 1, 1, 1 ]
     * ]
     *
     * adapted from : https://stackoverflow.com/questions/27205018/multiply-2-matrices-in-javascript
     * more about matrix multiplication: 
     * - https://en.wikipedia.org/wiki/Matrix_multiplication#Definition
     * - (clearer) https://fr.wikipedia.org/wiki/Produit_matriciel
     * - (clearer) https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm 
     */
    const aNumCol = a[0].length,  // number of columns in A
          aNumRow = a.length,     // number of rows in A, also size of the matrix
          bNumCol = b[0].length,  // number of cols in B
          bNumRow = b.length;     // number of rows in B
    let out = new Array(aNumRow);
    
    if ( aNumCol != bNumRow ) {
      throw new RangeError(`
        invalid matrices: 
        a_size must be of (n*m) and 
        b_size must be of (m*p).
        
        instead, got:
        a_size: (${aNumRow}*${aNumCol}) 
        b_size: (${bNumRow}*${bNumCol})
      `)
    }
    
    let i,j,k;
    for ( i=0; i<aNumRow; i++ ) {
      out[i] = new Array(bNumCol)
      for ( j=0; j<bNumCol; j++ ) {
        out[i][j] = 0;
        for ( k=0; k<aNumCol; k++ ) {
          out[i][j] += a[i][k] * b[k][j];
        }
      }
    }
    return out
  }
  
  async function clickOrScroll(el) {
    /**
     * for mobile devices: check if we have clicked or scrolled:
     * if in 0.1s, the page has moved up or down by more than 5px.
     *
     * to do so we must use a promise with a `setTimeout()` which returns
     * a value using the `resolve()` function. if there's an error, it's the
     * `reject()` which is fired. 
     * see: https://stackoverflow.com/questions/24928846/get-return-value-from-settimeout
     * 
     * @param {jQuery} el: the element from which to get the offset top
     * @return {bool}: true if click (scroll <= 5 px), false if scroll (scroll > 5px)
     */
    if ( 
      $(".burger").css("display") !== "none" 
      // && globals.currentToggling === false 
    ) {
      const start = el.offset().top;
      return new Promise((resolve, reject) => {
        setTimeout( () => {
          resolve( Math.abs( start - el.offset().top ) <= 5 );
          reject(false);
        }, 100 )
      });
    } else {
      return true;
    }
  }
  
  
  /* AJAX AND HTTP */
  async function svg() {
      /**
       * load svg components
       */
      const togglerL = await loadStaticsFile("img/svg/toggler_left.svg", dtype="xml");
      const togglerR = await loadStaticsFile("img/svg/toggler_right.svg", dtype="xml");
      const logo = await loadStaticsFile("img/svg/logo.svg", dtype="xml");
      const plus = await loadStaticsFile("img/svg/plus.svg", dtype="xml");
      $(".toggler-left").append(togglerL.documentElement);
      $(".toggler-right").append(togglerR.documentElement);
      $(".more .svg-container").append(plus.documentElement);
      $(".intro .svg-container").append(logo.documentElement);
      $(".intro-menu-container svg").css({ height: 0.8 * $(".intro-menu-container").height() });  // can't be done in css
  }
  
  function loadStaticsFile(fpath, dtype="json") {
    /**
     * load a file in the `statics` folder
     * using AJAX.
     * @param {string} fpath: the path of the file on `digital.inha.fr`,
     *                        from the `statics` folder.
     * @param {string} dtype: the response's datatype
     * @returns: the server response, which is the file if there's no error
     */
    const url = new URL("https://digital.inha.fr/richelieu/code/application_presentation/")
    return $.ajax({
      url: new URL(fpath, `${url}statics/`),
      crossOrigin: true,
      dataType: dtype,
      mimeType: "text/plain"  // avoid "badly formed xml" errors 
    }).fail((res, status, error) => {
      console.log(status, error, res);
    });
  }
  
  
  /* LEAFLET FUNCTIONS */
  function buildBackgroundTiles(richelieuZone) {
    /**
     * define all the background map tiles + geojson borders
     * of the neighbourhood
     * 
     * @param {geoJSON, object} richelieuZone: the geoJson of the neighbourhood's borders 
     */
    const rootUrls = {
      ignWms: "https://wxs.ign.fr/cartes/geoportail/r/wms",
      ignWmts: "https://wxs.ign.fr/cartes/geoportail/wmts"
    };
    return [
      L.tileLayer(
        // base stamen tile
        "https://stamen-tiles.a.ssl.fastly.net/toner/{z}/{x}/{y}.png", {
          pane: "basePane",
          minZoom: 3,
          maxZoom: 18,
          attribution: `Fonds de cartes par <a href='http://stamen.com'>` +
            `Stamen Design</a>, ` +
            `<a href='http://creativecommons.org/licenses/by/3.0'>` +
            `CC BY 3.0</a>`
        }
      )
      , L.tileLayer(
        // alpage-vasserot
        "https://tile.maps.huma-num.fr/uc2usU/d/Alpage_Vasserot_1830/{z}/{x}/{y}.png", {
          opacity: 0.85
        }
      ),L.tileLayer(
        // plan 1900
        "https://tile.maps.huma-num.fr/uc2usU/d/MOSA_1900_PARIS/{z}/{x}/{y}.png", {
          opacity: 0.85
        }
      ), L.geoJSON(
        // the borders of the neighbourhood
        richelieuZone, style = {
          color: "var(--cs-lfl)",
          stroke: "#ffffff",
          fillOpacity: 0.5,
          opacity: 1,
        }, pane = "basePane", interactive = false
      )
    ]
  }
  
  /* OPENSEADRAGON FUNCTIONS */
  async function processManifest(manifestUrl) {
    /**
     * process a IIIF manifest to extract the image url
     *
     * @param {string} manifestUrl: the IIIF manifest's url for the image.
     */
    return $.ajax({
      url: manifestUrl,
      dataType: "json",
      mimeType: "application/json",
      crossOrigin: true,
  
    }).then((res) => {
      // build a `out`, an array containing 
      // all the data necessary to create our iiif viewers
      const out = [];
      res.sequences[0].canvases.forEach((c) => {
        const url = c.images[0]
                      ["resource"]
                      ["@id"]
                      .replace(/\/full\/full\/[,\d]+\//, "/full/max/0/");
        const tileSequence = {
          type: "image",
          url: url,
          height: 1
        };
        out.push(tileSequence);
      });
      return out;
  
    }).fail((res, status, error) => {
      console.log(status, error, res);
  
    });
  }
  
})

