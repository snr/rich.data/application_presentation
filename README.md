# Application de présentation du projet Richelieu. Histoire du quartier

Cette application en une page permet de présenter les objectifs du programme de
recherche *Richelieu. Histoire du quartier*. Elle présente notre discours scientifique
et met en oeuvre les technologies centrales au projet (cartographie interactive avec *Leaflet*,
utilisation du IIIF et d'*Openseadragon*...).

L'application est entièrement responsive et pensée à la fois pour une utilisation sur écran, 
tablette et mobile.

---

## Utilisation

Il suffit de cloner le dépôt Git et d'ouvrir le fichier `index.html` dans un navigateur:

- Avoir une connexion internet.

- Dans un terminal, entrer les commandes:
  ```bash
  https://gitlab.inha.fr/snr/rich.data/application_presentation.git
  cd application_presentation/
  ```
- Ouvrir `index.html` dans un navigateur web.

---

## Liens

- [application mise en ligne](https://quartier-richelieu.inha.fr/) (et le lien sur 
  [les serveurs de l'inha](https://digital.inha.fr/richelieu/code/application_presentation/)
- [version 0.1](https://digital.inha.fr/richelieu/code/application_presentation_v0_1/)
- [version 0.1 revue par Jean-Christophe Carius](https://digital.inha.fr/richelieu/code/site_prefiguration_proto_2/)

---

## Crédits

- Données du programme Richelieu: Charlotte Duvette, Justine Gain, Esther Dasilva, 
Louise Baranger, Loïc Jeanson
- Cartographie réalisée pour le projet Richelieu. Histoire du quartier: Loic Jeanson,
  Colin Prudhomme
- Modélisation 3D: Colin Prudhomme
- Conception graphique et développement de l'application: Paul Kervegan

Le code de l'application est diffusé sous licence ouverte GNU GPL 3.0, les données et métadonnées
sont disponibles sous la licence Creative Commons Attribution 4.0 (CC-BY 4.0).
